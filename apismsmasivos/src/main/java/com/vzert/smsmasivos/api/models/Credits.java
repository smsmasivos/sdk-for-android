package com.vzert.smsmasivos.api.models;

public class Credits {

    private String success;
    private String message;
    private int status;
    private String code;
    private String credit;

    public String getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getCredit() {
        return credit;
    }

}
