package com.vzert.smsmasivos.api.models;

public class Request {

    private String success;
    private String message;
    private int status;
    private String code;

    public String getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

}
